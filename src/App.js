import data from "./colors.json";
import "./App.css";

function App() {
  return (
    <div>
      {Object.keys(data).map((color) => (
        <ColorDiv info={color} />
      ))}
    </div>
  );
}

function ColorDiv(props) {
  const str = props.info;
  const str2 = str.charAt(0).toUpperCase() + str.slice(1);
  return (
    <>
      <div className="div" style={{ fontSize: "20px" }}>
        {str2}
      </div>
      <div className="belowOfColor">colors.{props.info}</div>
      <div
        style={{
          position: "relative",
          lineHeight: "10px",
          left: "150px",
          paddingBottom: "50px",
          width: "1200px",
        }}
      >
        {data[props.info].map((color1, index) => (
          <Child info={color1} index={index} />
        ))}
      </div>
    </>
  );
}

function Child(props) {
  return (
    <>
      <div className="chiled" style={{ backgroundColor: String(props.info) }}>
        <div className="colorValue">{props.info.toUpperCase()}</div>
        <div className="value">
          {props.index == 0 ? "50" : props.index * 100}
        </div>
      </div>
    </>
  );
}

export default App;
